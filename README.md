# README #

ESP-01 based alarm / sensor input for EasyIoT

### What is this repository for? ###

* ESP-01 module to take a switched input and generate an EasyIoT alarm
  * Configuration button (GPIO3/RxD) to set WiFi configuration
* Version 1.0


### How do I get set up? ###

* Project based on PlatformIO IDE but could be used as a standard Arduino ESP project
* Configuration
  * Uses XML Alarm PCB
  * 5-15v supply
  * ESD protected switch debounce chip on the alarm input
  * Configuration button to set up WiFi & EasyIoT parameters
+ Dependencies
  * https://github.com/tzapu/WiFiManager
* Deployment instructions
  * Plug a programmed ESP-01 / ESP-01S into the PCB
  * Apply power - the status LED should flash slowly indicating the unit hasn't
  connected to the network
  * Press the CFG button and the status LED will switch to a rapid flash
  * At this point the unit is configured as an Access Point and you can connect
  from a mobile phone, laptop etc
  * Go to the WiFi settings and look for an SSID starting XMLA eg XMLA123456 and
  connect to this WiFi. The password is the same as the SSID
  * When you are connected, go to a browser and enter the IP address 192.168.4.1
  and select Configure WiFi
  * You can now set the WiFi credentials for your home network and all the
  EasyIoT parameters (server, port, username, password and node)

### Who do I talk to? ###

* c.blood@risch.co.uk
