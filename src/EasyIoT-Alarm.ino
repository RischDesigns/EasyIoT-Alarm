/*******************************************************************************
* EasyIoT-Alarm.ino
*
* Uses ESP-01 GPIO2 input to generate an EasyIoT alarm. All the EasyIoT
* parameters are saved in the SPIFFs filing system
* Uses wiFiManager to manage WiFi/password connectivity and to set the EasyIoT
* alarm parameters. On-demand configuration is triggered using GPIO3 (Rx pin)
*
* Status is indicated by an LED connected to GPIO0
* No local display - uses LED to indicate status
* 	- fast flash - config mode can be accessed as WiFi AP
* 	- slow flash - booting / trying to connect
* 	- solid on - connected & reporting data
*
*******************************************************************************/
#include <Arduino.h>

#include <FS.h>
#include <stdlib.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include "Base64.h"

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include "WiFiManager.h"          //https://github.com/tzapu/WiFiManager

#include <Ticker.h>

#define STATE_LED 0   // GPIO 0
#define ALARM_PIN 2 // GPIO 2
#define TRIGGER_PIN 3   // GPIO 3 (RxD, function 3)
#define MODE_NORMAL 0
#define MODE_ALARM 1

#define EASYIOT_SALT 10082017
#define USER_PWD_LEN      40
// un-comment the following line to debug but the Rx pin is used as the config pin
//#define SERIAL_OUT 1

// --------------- constants -------------------
const char *product = "XMLA";
const char *version = "Risch XMLA (EasyIoT) v1.0";

typedef struct _easy_iot {
	char server[40];       // EasyIoT server name
    unsigned short port;  // EasyIoT port number
	char user[32]; 	      // EasyIoT server username
    char pass[32];        // EasyIoT server password
 	char node[32];        // EasyIoT node
    int salt;
} EasyIoT;

// --------------local variables ------------------
int send_alarm;
int mode;
unsigned long alarm_timer;
bool shouldSaveConfig = false;
char unameenc[USER_PWD_LEN];

String chipID;
Ticker ticker;
EasyIoT easyIoT;
WiFiClient  client;

/****************************************************************************
Prototype	  :	void tick(void)

Procedure Desc:	Callback function from ticker to flash the LED

	Params	  :	None
	Return	  :	None
	Globals   :	None
****************************************************************************/
void tick()
{
  //toggle state
  int state = digitalRead(STATE_LED);  // get the current state of GPIO pin
  digitalWrite(STATE_LED, !state);     // set pin to the opposite state
}

/****************************************************************************
Prototype	  :	void saveConfigCallback (void)

Procedure Desc:	Callback function if additional parameters have been submitted

	Params	  :	None
	Return	  :	None
	Globals   :	Sets shouldSaveConfig
****************************************************************************/
//callback notifying us of the need to save config
void saveConfigCallback ()
{
#ifdef SERIAL_OUT
  Serial.println("Should save config");
#endif
  shouldSaveConfig = true;
}

/****************************************************************************
Prototype	  :	void save_easyiot_config(void)

Procedure Desc:	Saves additional parameters in JSON format file on SPIFFS

	Params	  :	None
	Return	  :	None
	Globals   :	Reads settings structure
****************************************************************************/
void save_easyiot_config(void)
{
#ifdef SERIAL_OUT
    Serial.println("saving EasyIoT config");
#endif
    // create a JSON object to write to the file
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["Server"] = easyIoT.server;
    json["Port"] = String(easyIoT.port);
    json["Username"] = easyIoT.user;
    json["Password"] = easyIoT.pass;
    json["Node"] = easyIoT.node;
    json["Salt"] = EASYIOT_SALT;

    // create / update the file
    File configFile = SPIFFS.open("/easyiot-config.json", "w");
#ifdef SERIAL_OUT
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }
    json.printTo(Serial);
#endif
    json.printTo(configFile);
    configFile.close();
#ifdef SERIAL_OUT
    Serial.println();
#endif
    //end save
}

/****************************************************************************
Prototype	  :	void alarm_interrupt(void)

Procedure Desc:	Interrupt handler for alarm input pin

	Params	  :	None
	Return	  :	None
	Globals   :	Reads settings structure
****************************************************************************/
void alarmInterrupt(void)
{
    int alarm_input;

	alarm_input = digitalRead(ALARM_PIN);
	if (alarm_input)
	{
		alarm_timer = millis();
        alarm_timer += 3000;
	}
	else
	{
		if (MODE_ALARM != mode)
		{
#ifdef SERIAL_OUT
			Serial.println(F("Notify"));
#endif
			send_alarm=1;
		}
		mode = MODE_ALARM;
		alarm_timer = 0;
	}
}

/****************************************************************************
Prototype	  :	void sendEasyIoTAlarm(void)

Procedure Desc:	Post the EasyIoT alarm message

	Params	  :	None
	Return	  :	None
	Globals   :	Reads settings structure
****************************************************************************/
void SendEasyIoTAlarm(bool inputState)
{
    WiFiClient easyIoTclient;

    if (EASYIOT_SALT != easyIoT.salt)
    {
#ifdef SERIAL_OUT
        Serial.println(F("Invalid EasyIoT configuration"));
#endif
    }
    else if (!easyIoTclient.connect(easyIoT.server, easyIoT.port)) {
#ifdef SERIAL_OUT
        Serial.println(F("Alarm connection failed"));
#endif
    }
    else
    {
        // turn the LED on constant
        ticker.detach();
        digitalWrite(STATE_LED, HIGH);

        String url = "";
        String command = "";

        if (inputState)
         command = "ControlOn";
        else
         command = "ControlOff";

        url = "/Api/EasyIoT/Control/Module/Virtual/"+ String(easyIoT.node) + "/"+command; // generate EasIoT server node URL
#ifdef SERIAL_OUT
        Serial.print("POST data to URL: ");
        Serial.println(url);
#endif
        easyIoTclient.print(String("POST ") + url + " HTTP/1.1\r\n" +
                    "Host: " + String(easyIoT.server) + "\r\n" +
                    "Connection: close\r\n" +
                    "Authorization: Basic " + unameenc + " \r\n" +
                    "Content-Length: 0\r\n" +
                    "\r\n");

        delay(100);
         while(easyIoTclient.available()){
         String line = easyIoTclient.readStringUntil('\r');
#ifdef SERIAL_OUT
         Serial.print(line);
#endif
        }

#ifdef SERIAL_OUT
        Serial.println();
        Serial.println("Connection closed");
#endif
   }
}

/****************************************************************************
Prototype	  :	void setup(void)

Procedure Desc:	Main configuration - runs once on startup

	Params	  :	None
	Return	  :	None
	Globals   :
****************************************************************************/
void setup(void) {
#ifdef SERIAL_OUT
  Serial.begin(115200);
  Serial.println();
  Serial.println("EasyIoT-Alarm"); // name of Project
#endif
  //set led pin as output
  pinMode(STATE_LED, OUTPUT);
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  // set alarm pin as Input
  pinMode(ALARM_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(ALARM_PIN), alarmInterrupt, CHANGE);
  mode = MODE_NORMAL ;

  // set the RxD pin as GPIO for alarm trigger
  pinMode(TRIGGER_PIN, FUNCTION_3);

#ifdef SERIAL_OUT
  Serial.println("Mounting filesystem");
#endif
  if (SPIFFS.begin())
  {
#ifdef SERIAL_OUT
    Serial.println("mounted file system");
    listFiles(); // Lists the files so you can see what is in the SPIFFS
#endif
    if (SPIFFS.exists("/easyiot-config.json")) {
      //file exists, reading and loading
#ifdef SERIAL_OUT
      Serial.println("reading EasyIoT config file");
#endif
      File configFile = SPIFFS.open("/easyiot-config.json", "r");
      if (configFile) {
#ifdef SERIAL_OUT
        Serial.println("opened EasyIoT config file");
#endif
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
#ifdef SERIAL_OUT
        json.printTo(Serial);
#endif
        if (json.success()) {
#ifdef SERIAL_OUT
          Serial.println("\nparsed json");
#endif
          if ( EASYIOT_SALT == json["Salt"])
          {
              strcpy(easyIoT.server,json["Server"]);
              easyIoT.port = json["Port"];
			  strcpy(easyIoT.user, json["Username"]);
			  strcpy(easyIoT.pass, json["Password"]);
              strcpy(easyIoT.node, json["Node"]);
              easyIoT.salt = json["Salt"];
          }
		}
	  }
	}
  }
#ifdef SERIAL_OUT
  Serial.println("Read SPIFFS files");
#endif
  // set Chip ID - used for AP name & password
  chipID = product + String(ESP.getChipId());

#ifdef SERIAL_OUT
  Serial.println("EasyIoT structure");
#endif
  if (EASYIOT_SALT == easyIoT.salt)
  {
#ifdef SERIAL_OUT
      Serial.println(easyIoT.server);
      Serial.println(easyIoT.port);
//    Serial.println(easyIoT.user);
//    Serial.println(easyIoT.pass);
      Serial.println(easyIoT.node);
#endif
      // create the encoded username/password
      char uname[USER_PWD_LEN];
      String str = String(easyIoT.user)+":"+String(easyIoT.pass);
      str.toCharArray(uname, USER_PWD_LEN);
      memset(unameenc,0,sizeof(unameenc));
      base64_encode(unameenc, uname, strlen(uname));

  }
#ifdef SERIAL_OUT
  else
    Serial.println("not initialised");

  Serial.println(WiFi.localIP());
  Serial.println(WiFi.SSID());
#endif
}

/****************************************************************************
Prototype	  :	void loop(void)

Procedure Desc:	Main processing loop

	Params	  :	None
	Return	  :	None
	Globals   :	Reads settings structure
****************************************************************************/
void loop(void) {
	unsigned long current_timestamp;
    // is configuration portal requested?
    if ( digitalRead(TRIGGER_PIN) == LOW ) {
      WiFi.disconnect();
      //entered config mode, make led toggle faster
	  ticker.attach(0.2, tick);

      //WiFiManager
      //Local intialization. Once its business is done, there is no need to keep it around
      WiFiManager wifiManager;

      char parameter[32];
      // custom parameters
      WiFiManagerParameter custom_easyiot_title("<br/>EasyIoT config. <br/>");
      WiFiManagerParameter custom_easyiot_server("easyiot_server", "EasyIoT server", easyIoT.server, 40);
      sprintf(parameter,"%d", easyIoT.port);
      WiFiManagerParameter custom_easyiot_port("easyiot_port", "EasyIoT port", parameter, 6);
      WiFiManagerParameter custom_easyiot_user("easyiot_username", "EasyIoT user", easyIoT.user, 32);
      WiFiManagerParameter custom_easyiot_pass("easyiot_password", "EasyIoT pass", easyIoT.pass, 32);
      WiFiManagerParameter custom_easyiot_node("easyiot_node", "EasyIoT node", easyIoT.node, 32);

      wifiManager.addParameter(&custom_easyiot_title);
      wifiManager.addParameter(&custom_easyiot_server);
      wifiManager.addParameter(&custom_easyiot_port);
      wifiManager.addParameter(&custom_easyiot_user);
      wifiManager.addParameter(&custom_easyiot_pass);
      wifiManager.addParameter(&custom_easyiot_node);
      //set config save notify callback
      wifiManager.setSaveConfigCallback(saveConfigCallback);

      //sets timeout until configuration portal gets turned off
      //useful to make it all retry or go to sleep
      //in seconds
      wifiManager.setTimeout(300);

      if (!wifiManager.startConfigPortal(chipID.c_str(), chipID.c_str())) {
#ifdef SERIAL_OUT
        Serial.println("failed to connect and hit timeout");
#endif
        delay(3000);
        //reset and try again, or maybe put it to deep sleep
        ESP.reset();
        delay(5000);
      }

      //if you get here you have connected to the WiFi
#ifdef SERIAL_OUT
      Serial.println("connected... :)");
#endif
      ticker.detach();
      // turn the LED on constant
      digitalWrite(STATE_LED, HIGH);

#ifdef SERIAL_OUT
      Serial.println("local ip");
      Serial.println(WiFi.localIP());

      Serial.println(chipID);
#endif
      if (true == shouldSaveConfig)
      {
		  strcpy(easyIoT.server, custom_easyiot_server.getValue());
          easyIoT.port = atoi(custom_easyiot_port.getValue());
          strcpy(easyIoT.user, custom_easyiot_user.getValue());
          strcpy(easyIoT.pass, custom_easyiot_pass.getValue());
          strcpy(easyIoT.node, custom_easyiot_node.getValue());
          easyIoT.salt = EASYIOT_SALT;
          save_easyiot_config();

          // update the encoded username/password
          char uname[USER_PWD_LEN];
          String str = String(easyIoT.user)+":"+String(easyIoT.pass);
          str.toCharArray(uname, USER_PWD_LEN);
          memset(unameenc,0,sizeof(unameenc));
          base64_encode(unameenc, uname, strlen(uname));

      }

      // reset the interval so we send a reading immediately
    }

 	current_timestamp = millis();
 	switch (mode)
	{
		default:
		case MODE_NORMAL:
			break;
		case MODE_ALARM:
			if (send_alarm)
			{
				send_alarm = 0;
                SendEasyIoTAlarm(true);
			}
			if (alarm_timer && (current_timestamp >= alarm_timer))
			{
                SendEasyIoTAlarm(false);
#ifdef SERIAL_OUT
 				Serial.println(F("Alarm timer expired"));
#endif
				mode = MODE_NORMAL;
				alarm_timer = 0;
			}
			break;
	}
	delay(10);

}

/****************************************************************************
Prototype	  :	void listFiles (void)

Procedure Desc:	Directory listing of the SPIFFS

	Params	  :
	Return	  :
	Globals   :
****************************************************************************/
void listFiles(void) {
#ifdef SERIAL_OUT
  Serial.println();
  Serial.println("SPIFFS files found:");

  Dir dir = SPIFFS.openDir("/"); // Root directory
  String  line = "=====================================";

  Serial.println(line);
  Serial.println("  File name               Size");
  Serial.println(line);

  while (dir.next()) {
    String fileName = dir.fileName();
    Serial.print(fileName);
    int spaces = 25 - fileName.length(); // Tabulate nicely
    while (spaces--) Serial.print(" ");
    File f = dir.openFile("r");
    Serial.print(f.size()); Serial.println(" bytes");
  }

  Serial.println(line);
  Serial.println();
  delay(1000);
#endif
}
